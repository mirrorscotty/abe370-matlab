function z = conv_numerical(x1, y1, x2, y2)
% This calculates the convolution integral of two x-y data sets. The first set
    % of variables corresponds to the x and y values from the first function,
    % and x2 and y2 are the x-y values from the second function. The y values
    % for these functions are assumed to be zero outside of the supplied domain.

% Determine the min and max x values for each function. This is mostly to make
% the code later on more concise.
x1min = x1(1);
x1max = x1(length(x1));
x2min = x2(1);
x2max = x2(length(x2));

% Interpolate each function using cubic splines and save the result as a set of
% piecewise polynomials.
pp1 = spline(x1, y1);
pp2 = spline(x2, y2);

% Find the average length of x1 and x2. This is to determine how many values
% should be included in the final result.
avglen = 0.5 * (length(x1)+length(x2));

% Only integrate where the domains intersect.
if(x1min>x2min)
    xx1 = x1min;
else
    xx1 = x2min;
end
if(x1max > x2max)
    xx2 = x2max;
else
    xx2 = x1max;
end

% Create a new set of x values to integrate over.
xx = linspace(xx1, xx2, avglen);

% Calculate the y values for each function over the new domain using the
% previously generated cubic splines.
yy1 = ppval(pp1, xx);
yy2 = ppval(pp2, xx);

% Multiply all the values together.
yy = yy1 .* yy2;

% Integrate using the trapezoid rule.
z = trapz(xx, yy);

