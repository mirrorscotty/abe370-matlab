function [x, fval] = fmaxbnd(func, x1, x2)
% Calculate the maximum of a function over a given interval
        % The argument "func" is the function handle of the equation to
        % maximize and x1 and x2 bound the region to search for the maximum
        % in.

% This line simply multiplies the original function by negative one and
% sends everything to matlab's built-in minimization function.
[x, fval] = fminbnd(@(x)(func(x)*(-1)), x1, x2);

% Multiply the function value at the maximum by -1 again to get the correct
% sign.
fval = -1*fval;
