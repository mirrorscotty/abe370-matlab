function [yy, xx] = make_splines(x, y, n, x0, x1)
% Function to interpolate a set of data using cubic splines.
        % The input variables x and y are each vectors that contain the set
        % of data to interpolate and n is the number of times more points
        % the output data should have than the input data does. The variables
        % x0 and x1 define the bounds to interpolate over. If these lie outside
        % the supplied x values, then the new y values will be set to zero.

% See if we need to shrink the initial domain because x0 is too large.
if(x0 > x(1))
    xstart = x0;
else
    xstart = x(1);
end

% Do the same if x1 is too small.
if(x1 < x(length(x)))
    xfinal = x1;
else
    xfinal = x(length(x));
end


% This simply creates a new x vector with n times more points than the
% original so that it can be fed into the spline function.
dx = (xfinal-xstart)/(length(x)*n);
xx = xstart:dx:xfinal;

% This function takes the original set of x and y data and a vector
% containing new x values to interpolate y values for. The output contains
% a vector of y values (one for each xx value) that were interpolated using
% cubic splines.
yy = spline(x, y, xx);

% Add in points that are outside of the original domian as zeros

% Do we need to add anything to the left end of the graph?
if(xx(1)>x0)
    % Number of extra values to add in
    nzeros = (xx(1) - x0)/dx;
    % Create a new matrix with the extra x values
    for i = 1:nzeros
        xbegin(i) = xx(1)-(nzeros-i+1)*dx
    end
    % Add in all the values created for x and zeros for y
    xx = [xbegin xx];
    yy = [zeros(1,nzeros) yy];
end

% This basically does the same thing as the above chunk of code, but on the
% other end of the graph.
if(xx(length(xx))<x1)
    nzeros = (x1 - xx(length(xx)))/dx;
    for i = 1:nzeros
        xend(i) = xx(length(xx)) + i*dx;
    end
    xx = [xx xend];
    yy = [yy zeros(1,nzeros)];
end
        
