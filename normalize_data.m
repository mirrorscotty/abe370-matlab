function [y] = normalize_data(Y, X)
% Function to take a set of x-y data and make the area under it equal one
        % Here, Y is an vector containing independent variable values to
        % be normalized, and X contains the dependent variable values. The
        % output is a vector of values for the independent variable for
        % which the area underneath has been normalized to one.

% The trapz function calculates the integral under the curve defined by the
% set of x-y data passed to it using the trapazoid rule for numerical
% integration.
z = trapz(X,Y);

% Divide each value of the Y vector by the area under the curve.
y = Y/z;

