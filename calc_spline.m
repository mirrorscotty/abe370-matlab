x = linspace(0, 1, 100);
Y = sin(x);

y = normalize_data(Y, x);

[yy, xx] = make_splines(x, Y, 10);

plot(xx, yy);

