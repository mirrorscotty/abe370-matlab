function dy = odesystem(x, y)
% Function to define the system of ODEs
%       This is used in the built-in equation solver to calculate all the y
%       values for a specified interval. Here, x is the dependent variable,
%       and y is a vector representing the independent variables. In this
%       example, those are y1, y2, and y3.

% Start by setting a variable equal to a column vector of zeros equal to
% the number of differential equations in the system.
dy = zeros(3,1);

% Equation 1: y1' = y2 * y3 + x
dy(1) = y(2) * y(3) + x;

% Equation 2: y2' = -y1 * y3
dy(2) = -y(1) * y(3);

% Equation 3: y3' = -0.51 * y1 * y2
dy(3) = -0.51 * y(1) * y(2);
