% Run this script to actually solve the system of ODEs defined in the other
% file. 

% This function (ode45) actually solves the system of equations. Here, the
% system defined in the 'odesystem' function is solved over the interval
% x=0 to x=12, with initial conditions of y1=0, y2=1, and y3=1. X and Y are
% the output variables that contain the solution. X is a vector containing
% the independent variable, and each column of the Y matrix contains the
% values of one of the independent variables.
[X, Y] = ode45(@odesystem, [0, 12], [0, 1, 1]);

% Make Matlab plot everything on one graph.
hold on;
% Plot y1
plot(X, Y(:,1), '-');

% Plot y2
plot(X, Y(:,2), '-.');

% Plot y3
plot(X, Y(:,3), '.');

hold off;
