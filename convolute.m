function z = convolute(f, g, x1, x2)
f(1)
c =@(x)(f(x)*g(x));
c(1)
z = integral(c, x1, x2);

